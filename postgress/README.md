# Install Postgress

## Usage:
```sh
sudo ./install.sh
```

## Configure:
```sh
# set postgres password
sudo -u postgres createuser postgres -s
sudo -u postgres psql
# postgres=# \password postgres
# Enter new password: password
# postgres=# \q
sudo service postgresql restart
```