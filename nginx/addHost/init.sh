#!/bin/bash

CUR_DIR=`pwd`

cd $CUR_DIR
source ./setKeys.sh

echo "---"
echo "Import this key into git web-interface, then continue."
read -p 'Press [Enter] to continue...' letsgo

cd $CUR_DIR
source ./addHost.sh

cd $CUR_DIR
source ./addRepo.sh