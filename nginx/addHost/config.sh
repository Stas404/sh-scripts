#!/bin/bash

export WWW_HOST=$1

# System vars (defaults for ubuntu)
export WWW_DIR=/var/www
export WWW_USER=www-data
export WWW_GROUP=www-data
export NGINX_DIR=/etc/nginx
export SSH_DIR=/var/www/.ssh

# Webhook info
export WEBHOOK_DIR=webhook
export WEBHOOK_FILE=index.php

# Repository info (change here)
export REPO_GIT=git@bitbucket.org:Stas404/w4spa.git
export REPO_NAME=w4spa
export REPO_PROD=$REPO_NAME/app/build