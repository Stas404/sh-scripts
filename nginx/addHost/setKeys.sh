#!/bin/bash
source ./config.sh

# ---------------------
echo "--- Set ssh-keys"
# ---------------------

echo "[1] Create .ssh-dir '$SSH_DIR'"
if [ -d $SSH_DIR ]; then
	echo "'.ssh' already exists."
	echo "[2] Your public key: $SSH_DIR/id_rsa.pub"
	sudo -u $WWW_USER cat $SSH_DIR/id_rsa.pub
	return
fi
sudo mkdir $SSH_DIR

echo "[2] Update permissions for '.ssh' (owner: $WWW_USER)"
sudo chown -R $WWW_USER:$WWW_GROUP $SSH_DIR

echo "[3] Generating public/private rsa key pair"
sudo -u $WWW_USER echo -e  'y\n' | ssh-keygen -q -t rsa -N "" -f $SSH_DIR/id_rsa > /dev/null
sudo chown -R $WWW_USER:$WWW_GROUP $SSH_DIR

echo "[4] Your public key: $SSH_DIR/id_rsa.pub"
sudo -u $WWW_USER cat $SSH_DIR/id_rsa.pub
