# Setup new nginx vhost

* Create nginx vhost
* Generate ssh-keys (if not exists)
* Clone repo
* Setup webhook entry (for automatic `git pull`)

# Using

* Edit `config.sh`
* Start script: `sudo ./init.sh domain.name` (or execute scripts separately for your needs)

## TODO:
* Random name for webhook dir
* More CLI options (vhost, repo_name, repo_git)
* Prompts (interactive script)