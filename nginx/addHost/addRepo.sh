#!/bin/bash
source ./config.sh

# --------------------------
echo "--- Import repository"
# --------------------------

echo "[1] Clone $REPO_GIT"
cd $WWW_DIR/$WWW_HOST
sudo -u $WWW_USER git clone $REPO_GIT $REPO_NAME
sudo chown -R $WWW_USER:$WWW_GROUP $REPO_NAME

#echo "[2] Initial git pull (answer 'yes' when prompt)"
#cd $REPO_NAME
#sudo -u $WWW_USER git pull

