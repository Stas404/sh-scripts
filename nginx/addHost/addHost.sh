#!/bin/bash
source ./config.sh

# ------------------------------
echo "--- Create dirs and files"
# ------------------------------

echo "[1] Create host-dir '$WWW_DIR/$WWW_HOST'"
if [ -d $WWW_DIR/$WWW_HOST ]; then
	echo "Host already exists. Exiting."
	return
fi
sudo mkdir $WWW_DIR/$WWW_HOST

echo "[2] Create webhook-dir"
cd $WWW_DIR/$WWW_HOST
sudo mkdir $WEBHOOK_DIR && cd $WEBHOOK_DIR
sudo cat <<END >$WEBHOOK_FILE
<style>
	* {margin:0; padding:0;}
	body {background:#111; font-family: "Courier New", Courier, monospace;}
	dl {margin:12px; color:#fff;}
	dt {padding:12px; }
	strong {color: #5f5;}
	em {color: #333;}
	dd {white-space: pre; padding:12px; background:#000;}
</style>
<dl>
	<dt>$ <strong>git log</strong></dt>
	<dd><?=shell_exec('cd ../w4spa && git log --pretty=format:"%h: <strong>%s</strong> <em>— %an, %ar</em>" -5');?></dd>
</dl>
<dl>
	<dt>$ <strong>git pull</strong></dt>
	<dd><?=shell_exec('cd ../w4spa && /usr/bin/git pull 2>&1');?></dd>
</dl>
<dl>
	<dt>$ <strong>git log</strong></dt>
	<dd><?=shell_exec('cd ../w4spa && git log --pretty=format:"%h: <strong>%s</strong> <em>— %an, %ar</em>" -5');?></dd>
</dl>
END

echo "[3] Update permissions for $WWW_USER"
sudo chown -R $WWW_USER:$WWW_GROUP $WWW_DIR/$WWW_HOST

echo "[4] Done."



# --------------------------
echo "--- Create nginx-conf"
# --------------------------

echo "[5] Create conf for '$WWW_HOST'"
if [ -f $NGINX_DIR/sites-available/$WWW_HOST ]; then
	echo "Conf already exists. Exiting."
	return
fi

cd $NGINX_DIR/sites-available
sudo cat <<END >$WWW_HOST
server {
	listen 80;
	listen [::]:80;
	server_name $WWW_HOST;
	root /var/www/$WWW_HOST/$REPO_PROD;
	#include oz/digital;

# location for webhook
	location /webhook {
		root /var/www/$WWW_HOST;
		index index.php index.html index.htm;
		location ~ \.php$ {
			include snippets/fastcgi-php.conf;
			fastcgi_pass unix:/run/php/php7.0-fpm.sock;
			fastcgi_intercept_errors on;
		}
	}
}

# www-redirect
server {
	listen 80;
	server_name www.$WWW_HOST;
	return 301 $scheme://$WWW_HOST$request_uri;
}
END

echo "[6] Create symlink for 'sites-enabled'"
cd $NGINX_DIR/sites-enabled
sudo ln -s ../sites-available/$WWW_HOST .

echo "[7] Restart nginx"
sudo service nginx restart

echo "[8] Done."

echo "Update your /etc/hosts if needed:"
echo "127.0.0.1    $WWW_HOST"